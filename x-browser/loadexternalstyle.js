var assert = chai.assert;

describe("Load external stylesheet from JavaScript.", function() {

  beforeEach(function(done) {
    console.log('create style from external stylesheet');

    var head = document.head || document.getElementsByTagName('head')[0];

    // remove the current stylesheets
    //while (document.getElementsByTagName('style').length > 0) {
    //  head.removeChild(document.getElementsByTagName('style')[0]);
    //}

    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = 'base/x-browser/style.css';
    link.media = 'all';
    head.appendChild(link);

    done();
  });

  it("Test that stylesheet has been loaded.", function() {
    var styleProp = 'font-size';

    // Add a header that we can check the style for
    var el = document.createElement('h1');
    el.innerHTML = 'This is a header 2';
    el.id = 'header2';
    el.className = 'myclass2'
    document.documentElement.appendChild(el);

    var x = document.getElementById('header2');
    var fontSize;
    if (x.currentStyle)
    fontSize = x.currentStyle[styleProp];
    else if (window.getComputedStyle)
    fontSize = document.defaultView.getComputedStyle(x, null).getPropertyValue(styleProp);

    assert.equal(fontSize, '32px', 'external stylesheet not executed correctly');
  });

});
