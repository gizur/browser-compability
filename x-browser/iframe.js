var assert = chai.assert;

describe("iframes", function() {

  beforeEach(function(done) {
      console.log('Create iframe');

      frame = document.createElement('iframe');
      frame.id = 'sample-frame';
      frame.name = 'sample-frame';

      frame.addEventListener('load', function(event) {
        console.log('load event');

        // These all works in Safari but not i Firefox:
        // * document.getElementById(frame.id).contentWindow.document
        // * document.getElementById(frame.id).contentDocument
        // * frame.contentDocument

        // this works in both Safari and Firefox (and Chrome)
        // don't know about Internet Explorer
        var doc = event.target.contentDocument;

        doc.body.innerHTML = 'hello world';
        doc.head.title = 'hello world';

        done();

      });

      document.documentElement.appendChild(frame);
  });

  it("test dynamically loaded iframe", function() {
    var doc = document.getElementById('sample-frame').contentWindow.document;

    assert.equal(doc.body.innerHTML, 'hello world',
      'body not updated correctly');

  });

});
