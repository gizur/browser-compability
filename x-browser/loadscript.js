var assert = chai.assert;

describe("load script from JavaScript", function() {

  beforeEach(function(done) {
    console.log('create script');

    var head = head || document.head ||
      document.getElementsByTagName('head')[0];

    var script = document.createElement('script');
    script.defer = true;
    script.async = false;
    script.text = 'console.log("hello world");' +
      'var d = document.createElement("div");' +
      'd.id="dynamic";' +
      'document.documentElement.appendChild(d);';

    head.appendChild(script);

    done();
  });

  it("test that script has executed", function() {
    assert.isNotNull(document.getElementById('dynamic'),
      'script not executed correctly');
  });

});
