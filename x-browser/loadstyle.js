var assert = chai.assert;

describe("Load stylesheet from JavaScript.", function() {

  beforeEach(function(done) {
    console.log('create style');

    var head = document.head || document.getElementsByTagName('head')[0];

    // remove the current stylesheets
    //while (document.getElementsByTagName('style').length > 0) {
    //  head.removeChild(document.getElementsByTagName('style')[0]);
    //}

    // create a new stylesheet
    var style = document.createElement('style');

    var css =
      'h1.myclass3 {' +
      '  font-weight: bold;' +
      '  color: rgb(255,0,0);' +
      '  font-size: 24px;' +
      '}';

    style.type = 'text/css';

    if (style.styleSheet) {
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }

    head.appendChild(style);

    done();
  });

  it("Test that stylesheet has been loaded.", function() {
    var styleProp = 'font-size';

    // Add a header that we can check the style for
    var el = document.createElement('h1');
    el.innerHTML = 'This is a header 1';
    el.id = 'header1';
    el.className = 'myclass3';
    document.documentElement.appendChild(el);

    var x = document.getElementById('header1');
    var y;
    if (x.currentStyle)
      y = x.currentStyle[styleProp];
    else if (window.getComputedStyle)
      y = document.defaultView.getComputedStyle(x, null).getPropertyValue(styleProp);

    //console.log('x'+x+'x.currentStyle[styleProp]:'+document.defaultView.getComputedStyle(x, null).getPropertyValue(styleProp));
    assert.equal(y, '24px', 'stylesheet not executed correctly.');
  });

});
