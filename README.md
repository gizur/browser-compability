Browser compability tests
=========================

A collection of tests with the purpose of finding technologies that works
across browsers.

Repo structure:

 * `browser-specific` - a sandbox where different techniques are tested to see
    what works in different browsers
 * `x-browser` - techniques that are tested and works in all tested browsers

These are the most common browser engines:

 * WebKit - used by Safari, Chrome and Opera uses a fork (blink)
 * Gecko - used by Firefox
 * Trident (mshtml) - used by Internet Explorer

Browser engine [stats](http://gs.statcounter.com)


Development stack
-----------------

Contains:
* Testing tools: Mocha and Chai
* Karma setup for Chrome, Firefox and Safari
* SauceLabs setup for continous integration


Pre-requisites
-------------

* NodeJS needs to be installed. Preferably with
[nvm](https://github.com/creationix/nvm). I typically use the latest stable
node version. See with `nvm ls-remote`

* Bower for managing browser modules: `npm install -g bower`


Getting started
--------------

* Installation: `npm install; npm run-script init`
* Run tests: `npm run-script test`
* Setup the configuration in `setenv` for your SauceLabs account using `setenv.template`.
Run SauceLab tests: `npm run-script sauce`
* Check that your code follows th code style: `npm run-script style`
